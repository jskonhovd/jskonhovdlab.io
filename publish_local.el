;; publish_local.el --- Publish org-mode project on Gitlab Pages
;; Author: Jeff

;;; Commentary:
;; This script will convert the org-mode files in this directory into
;; html.


;;; emacs --batch --no-init-file --load publish_local.el --funcall org-publish-all
;;; Code:

;;(require 'package)
;;(package-initialize)
;;(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
;;(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;;(package-refresh-contents)
;;(package-install 'org-plus-contrib)
;;(package-install 'htmlize)

(require 'org)
(require 'ox-publish)

;; setting to nil, avoids "Author: x" at the bottom
(setq user-full-name nil)

(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-toc nil)

(setq org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-validation-link nil
      org-html-doctype "html5")

(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")

(setq my-blog-extra-head
      (concat
       "<link rel='stylesheet' href='_assets/css/org.css' /> \n"
       "<link rel='stylesheet' href='_assets/css/blog.css' /> \n"
       "<link rel='stylesheet' href='_assets/css/code.css' /> \n"
       "<link rel='stylesheet' href='_assets/css/main.css' />"))

(setq my-blog-extra-head-posts
      (concat
       "<link rel='stylesheet' href='../_assets/css/org.css' /> \n"
       "<link rel='stylesheet' href='../_assets/css/blog.css' /> \n"
       "<link rel='stylesheet' href='../_assets/css/code.css' /> \n"
       "<link rel='stylesheet' href='../_assets/css/main.css' />"))

(setq my-blog-header-file "../res/_layouts/header.html")

(defun my-blog-header (arg)
  (with-temp-buffer
    (insert-file-contents my-blog-header-file)
    (buffer-string)))

(setq my-blog-footer
      "<hr />\n
License: <a href= \"https://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA 4.0</a></p>\n
<p><a href= \"/about.html\"> Contact</a></p>\n")


(setq org-publish-project-alist
      `(("blog"
	 :components ("blog-posts", "blog-pages", "site-static"))	 
	("blog-posts"
	 :base-directory "org/posts"
	 :base-extension "org"
	 ;;:recursive t
	 :publishing-function org-html-publish-to-html
	 :publishing-directory "./public/posts"
	 ;;:exclude (regexp-opt '("README" "draft"))
	 :htmlized-source t

	 ;; the following removes extra headers from HTML output -- important!
	 :html-link-home "/"
	 :html-head nil ;; cleans up anything that would have been in there.
	 :html-head-extra ,my-blog-extra-head-posts
	 :html-head-include-default-style nil
	 :html-head-include-scripts nil
	 :html-viewport nil

	 ;;:html-format-drawer-function my-blog-org-export-format-drawer
	 :html-home/up-format ""
	 ;;:html-mathjax-options ,my-blog-local-mathjax
	 ;;:html-mathjax-template "<script type=\"text/javascript\" src=\"%PATH\"></script>"
	 :html-footnotes-section "<div id='footnotes'><!--%s-->%s</div>"
	 :html-link-up ""
	 :html-link-home ""
	 :html-preamble my-blog-header 
	 :html-postamble ,my-blog-footer

	 :auto-sitemap t
	 :sitemap-filename "blog.org"
	 :sitemap-file-entry-format "%d *%t*"
	 ;;:html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon.ico\"/>"
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 :sitemap-date-format "Published: %a %b %d %Y")
	("blog-pages"
	 :base-directory "org/pages/"
	 :base-extension "org"
	 :publishing-directory "./public"
	 :publishing-function org-html-publish-to-html
	 ;;:preparation-function my-blog-pages-preprocessor
	 ;;:completion-function my-blog-pages-postprocessor
	 :htmlized-source t

	 :with-author t
	 :with-creator nil
	 :with-date t

	 ;;:headline-level 4
	 ;;:section-numbers nil
	 ;;:with-toc nil
	 ;;:with-drawers t
	 ;;:with-sub-superscript nil ;; important!!
	 ;;:html-viewport nil ;; hasn't worked yet

	 ;; the following removes extra headers from HTML output -- important!
	 :html-link-home "/"
	 :html-head nil ;; cleans up anything that would have been in there.
	 :html-head-extra ,my-blog-extra-head
	 :html-head-include-default-style nil
	 :html-head-include-scripts nil
	 :html-viewport nil

	 ;;:html-format-drawer-function my-blog-org-export-format-drawer
	 :html-home/up-format ""
	 ;;:html-mathjax-options ,my-blog-local-mathjax
	 ;;:html-mathjax-template "<script type=\"text/javascript\" src=\"%PATH\"></script>"
	 :html-footnotes-section "<div id='footnotes'><!--%s-->%s</div>"
	 :html-link-up ""
	 :html-link-home ""
	
	 :html-preamble my-blog-header
	 :html-postamble ,my-blog-footer)
	("site-static"
	 :base-directory "org/res/"
	 :exclude "public/"
	 ;;:base-extension site-attachments
	 :base-extension  "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|gz\\|tar\\|zip\\|bz2\\|xz\\|tex\\|txt\\|html\\|scm\\|key\\|svg"
	 :publishing-directory "./public"
	 :publishing-function org-publish-attachment
	 :recursive t
	 )))

(provide 'publish)
;;; publish.el ends here
